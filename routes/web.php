<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//kt route -> / <- home / index.php



 
Route::get('/messages', 'MessagesController@getMessages');

Route::post('/contact/submit', 'MessagesController@submit');

// Route::get('/', 'pagesController@getHome'); // or
Route::get('/', function() {
     return view('index');
    // return view('pagesController@getHome');
});



Route::get('/contact', 'pagesController@getContact');

// Route::get('/about', 'pagesController@getAbout'); // or
Route::get('/about', function()
{
	return view('about');
});

Route::get('/portfolio', function()
{
	return view('portfolio');
});