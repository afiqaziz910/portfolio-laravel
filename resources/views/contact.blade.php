@extends('layouts.app')

@section('content')
	<h2>contact Page</h2>
	{!! Form::open(['url' => 'contact/submit']) !!}
    	<div class="form-group">
    		{{Form::label('name', 'Name')}}
    		{{Form::text('name','',array('class' => 'form-control', 'placeholder' => 'name'))}}
    	</div>
    	<div class="form-group">
    		{{Form::label('email', 'E-Mail Address')}}
    		{{Form::text('email','',array('class' => 'form-control', 'placeholder' => 'example@gmail.com'))}}
    		<!-- {{Form::text('email','example@gmail.com',['class' => 'form-control'])}} --><!--lepas email tu '' <- value -->
    	</div>
    	<div class="form-group">
    		{{Form::label('message', 'Massage')}}
    		{{Form::textarea('message','',array('class' => 'form-control', 'placeholder' => 'enter massage '))}}
    	</div>
    	<dir>
    		{{form::submit('Submit', ['class' => 'btn btn-primary'])}}
    	</dir>
	{!! Form::close() !!}
@endsection